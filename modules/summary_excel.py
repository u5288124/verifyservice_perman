


def write_excel_summary(node_service_dict,objfolderinput):
    from modules.mylib import loadSettingPath
    from openpyxl import Workbook
    from modules.Obj import objService_status,objSap_status,objSdp_status,objInterface_status

    wb = Workbook()
    ws_summary = wb.active
    ws_summary.title = "SUMMARY"
    ws_summary.append(['NODEIP','SERVICES'])
    output_save = loadSettingPath().folderoutput
    fileexcel_name = f'{output_save}/{objfolderinput.foldername}/@SUMMARY.xlsx'

    for nodeip,servicelist in node_service_dict.items():
        ws_summary.append([nodeip,len(servicelist)])
        worksheet_node = wb.create_sheet(nodeip)
        worksheet_node.append(['SERVICE-ID','SERVICE', 'ALL-SAP-OPER-UP','ALL-SDP-OPER-UP','ALL-INTERFACE-OPER-UP'])

        for eachservice in servicelist :
            all_sap_up = False
            all_sdp_up = False
            all_interface_up = False
            count_sap_up = 0
            count_sdp_up = 0
            count_interface_up = 0
            if isinstance(eachservice,objService_status):
                for eachsap in eachservice.sap :
                    if isinstance(eachsap,objSap_status):
                        # check all sap admin + oper : UP
                        print(f'service : {eachservice.serviceno} sap : {eachsap.fullsap} , oper : {eachsap.sap_operstate}')
                        if eachsap.sap_operstate == objSap_status.up_state :
                            count_sap_up += 1
                print(f'check sap : {count_sap_up}/{eachservice.sapcount}')
                if count_sap_up == int(eachservice.sapcount) :
                    all_sap_up = True
                for eacesdp in eachservice.sdp :
                    if isinstance(eacesdp,objSdp_status):
                        # check all sap admin + oper : UP
                        print(f'service : {eachservice.serviceno} sdp : {eacesdp.sdpno} , oper : {eacesdp.sdp_operstate}')
                        if eacesdp.sdp_operstate == objSdp_status.up_state :
                            count_sdp_up += 1
                print(f'check sdp : {count_sdp_up}/{eachservice.sdpcount}')
                if count_sdp_up == int(eachservice.sdpcount) :
                    all_sdp_up = True

                for eachinf in eachservice.interface :
                    if isinstance(eachinf,objInterface_status):
                        # check all sap admin + oper : UP
                        print(f'service : {eachservice.serviceno} interface : {eachinf.interfaceName} , oper : {eachinf.interface_oper_state}')
                        if eachinf.interface_oper_state == objInterface_status.up_state :
                            count_interface_up += 1
                print(f'check sap : {all_interface_up}/{len(eachservice.interface)}')
                if count_interface_up == len(eachservice.interface) :
                    all_interface_up = True
                if count_sap_up == 0 :
                    all_sap_up = '-'
                if count_sdp_up == 0 :
                    all_sdp_up = '-'
                if count_interface_up == 0 :
                    all_interface_up = '-'
            worksheet_node.append([eachservice.serviceno,eachservice.serviceType,all_sap_up,all_sdp_up,all_interface_up])
    wb.save(fileexcel_name)
def call_summaryexcel(objfolderinput):
    from modules.Obj import objFolderInput
    from modules.mylib import loadSettingPath,getservicedetail
    from os import listdir, getpid
    from re import search
    from netaddr import IPNetwork
    pid = getpid()
    node_service_dict = dict()

    if isinstance(objfolderinput, objFolderInput):
        output_save = loadSettingPath().folderoutput
        path_perservice = f'{output_save}/{objfolderinput.foldername}/{objfolderinput.folder_perservice}'
        for file in listdir(path_perservice):  # วนลูปเอาชื่อไฟล์ทั้งหมดออกมา แต่จะมากรองด้วย pattern ด้านล่าง
            matchip = search(r'(pre|post)_((?:\d+\.){3}\d+)_(epipe|vpls|vprn)_(.*)[.]txt', file)
            if matchip:  # 507850230_epipe_10.9.73.19
                nodeip = matchip.group(2)
                servicetype = matchip.group(3)
                serviceid = matchip.group(4)
                if nodeip not in node_service_dict.keys():
                    node_service_dict[nodeip] = []

                node_service_dict[nodeip].append(getservicedetail(filepath_per_service=f'{path_perservice}/{file}',serviceid=serviceid))

    write_excel_summary(node_service_dict=node_service_dict,objfolderinput=objfolderinput)


def readExcel(objfolderinput,):
    from openpyxl import load_workbook

    load_workbook()