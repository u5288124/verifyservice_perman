settingPath = 'modules/settingPath.json'
from pip._internal.utils import encoding
import logging
params = None
objsettingpath = None

def loadSettingPath():
    global objsettingpath
    from json import load
    from modules.Obj import settingpath
    if objsettingpath is not None :
        return objsettingpath
    fileinput = open(settingPath,mode="r")

    json_setting = load(fileinput)

    objsettingpath = settingpath()
    objsettingpath.folderinput = json_setting['folderinput']
    objsettingpath.folderoutput = json_setting['folderoutput']
    objsettingpath.fileauthen_cisco = json_setting['fileauthen_cisco']
    objsettingpath.fileauthen_sam = json_setting['fileauthen_sam']
    objsettingpath.folderlog = json_setting['folderlog']
    objsettingpath.folder_Authen = json_setting['folder_Authen']

    fileinput.close()
    return objsettingpath
def setupLog():
    logging.basicConfig(filename='app.log',filemode='w',encoding='utf-8',level=logging.DEBUG)

def loadAuthen(fileauthen_cisco,fileauthen_sam,folderauthen):
    import modules.encryption as enCRYPT
    from os.path import isfile
    from sys import exit
    from json import loads,load
    global params

    if not isfile(fileauthen_cisco) and not isfile(fileauthen_sam):
        logging.warning('''Credential file is missing
            Program Exit.....''')
        exit()
    enCRYPT.createKEY(folderauthen)
    key = enCRYPT.loadKEY(folderauthen)
    enCRYPT.encrypt(fileauthen_cisco, key)
    authen = enCRYPT.decrypt(fileauthen_cisco, key)
    authen = authen.decode("utf-8")
    params = loads(authen)
    params_sam = load(open(fileauthen_sam))
    params.update(params_sam)
    if params is None:
        logging.warning('''Cannot load Authentication info.''')

    return params

def sambb_chk(params):
    from paramiko import SSHClient,AutoAddPolicy
    from sys import exit
    session = None
    try:
        session = SSHClient()
        session.set_missing_host_key_policy(AutoAddPolicy())
        session.connect(hostname=params['sam_bbl_ip'], username=params['sam_bb1_usr'], password=params['sam_bb1_pass'])
        logging.info(f"SAM-BB: {params['sam_bbl_ip']} is reachable:")
    except Exception as e :
        logging.error(f"SAM-BB ({params['sam_bbl_ip']})not reachable!!! Please check your VPN connection")
        exit()
    finally:
        if isinstance(session,SSHClient) :
            session.close()

def chk_input(prefix):
    from os import listdir
    from modules.mylib import loadSettingPath
    from re import match
    settingpath = loadSettingPath()

    path = settingpath.folderinput
#    match_folder = ''
    mat = []
    for sub_dir in listdir(path):  # List all sub directory in input path
        try:  # Match regular expression to existing listed sub directory (pre or post)
            match_regex = match(r'((pre|post).*)',sub_dir)  # Select only folder that match input prefix
            # match_regex = re.match(r'(pre|post)-verify_(\d+.\d+.\d+.\d+)',sub_dir)  # Select only folder that match input prefix
        except:
            match_regex = None
        if match_regex and match_regex.group(2) == prefix:
            match_folder = match_regex.group(1)
            mat.append(match_folder)
    return mat

def loadINPUT():
    from modules.Obj import objFolderInput
    from os import listdir
    while True:
        print('''\nPlease Select your task: (Select 1 or 2 only)
            1. [PRE]-migration services verify on Cisco/Nokia devices
            2. [POST]-migration services verify on Nokia devices ''')
        prefix = input(' Select option: ')
        if prefix == '1':
            prefix = 'pre'
            break
        elif prefix == '2':
            prefix = 'post'
            break
        else:
            print("\nInput error!!! Please select only '1' or '2'")
            continue
    match_dir = chk_input(prefix)
    print('\nMatch input task are:')
    range_folder = 1
    choice = []
    for task in match_dir:
        print(' ' + str(range_folder) + '. ' + task)
        choice.append(int(range_folder))
        range_folder += 1

    selected_folder = []
    while True:
        try :

            print('Please select input nodes: 1 - ' + str(len(match_dir)) + ' or "a" for all node\n')
            option = input('Select option is: ')
            if option == 'a':  # If input 'a' mean execute all service owner node
                print('Execute all task....')
                print('The service owner nodes are:')
                selected_folder = match_dir
                break
            elif option.isdigit() :
                if int(option) in choice:  # If input number match with IP address of service owner
                    pick = int(option)
                    selected_folder = [match_dir[pick - 1]]
                    print(f'Selected folder : {selected_folder}')
                break
            else:  # if not match correct input
                print('Input error!!! Please input only option in range.')
                continue
        except Exception as e :
            logging.error(f'ERR : {str(e)}')

    result = []
    for eachfolder in selected_folder :

        objfolderinput = objFolderInput()
        objfolderinput.prefix = prefix
        objfolderinput.foldername = eachfolder+'/'
        folderinput = loadSettingPath().folderinput
        pathinput = folderinput + eachfolder
        filenodelist = listdir(pathinput)
        objfolderinput.filenodelist = filenodelist
        result.append(objfolderinput)
    return result

def parseinput_To_objNodecmd(objfolderinput,filenodecmd):
    from json import load
    from modules.Obj import objNodecmd

    objsettingpath = loadSettingPath()
    inputfolder = objsettingpath.folderinput
    pathfile = inputfolder+ objfolderinput.foldername + filenodecmd
    print(pathfile)
    result_json = load(open(pathfile))


    data = result_json["dict_cli"]
    devicetype = result_json["device_type"]
    ip = list(data.keys())[0]
    cli = []
    isCISCO = False
    for eachtype, cmd_cli in data[ip].items():
        cli += cmd_cli
    if result_json['vendor'] == 'CISCO':
        cli = ['terminal length 0', 'terminal width 200'] + cli
        cli.append('exit')
        cli.append('q')
        isCISCO = True
    elif result_json['vendor'] == 'NOKIA':
        cli = ['environment no more'] + cli
        cli.append('logout')
        # cli.append('q')
        cli.append('exit')
        isCISCO = False
    if str(result_json['ismoveGW']) == 'true':
        ismoveGW = True
    else:
        ismoveGW = False
    objnodecmd = objNodecmd()
    objnodecmd.nodeip = ip
    objnodecmd.commandlist = cli
    objnodecmd.devicetype = devicetype
    objnodecmd.vendor = result_json['vendor']

    return objnodecmd

def Call_SSH(objnodecmd):
    import modules.myssh as mySSH
    from os import getpid
    from paramiko import ssh_exception,Channel
    from modules.Obj import objNodecmd
    import time
    pid = getpid()

    # logging = setupLog()

    if isinstance(objnodecmd,objNodecmd):
        ip = objnodecmd.nodeip
        cli_list = objnodecmd.commandlist
        settings_authen = objnodecmd.autheninfo
        vendor = objnodecmd.vendor
        if str(vendor).lower() =='cisco' :
            isCISCO = True
        else :
            isCISCO = False

    output_command = ''
    logging.info(f"#######  [CONNECTING SAM-BB]  #######")

    print(f'[PID:{pid}] Start process "NODE {ip}/isCISCO:{isCISCO}"')

    channel, sum_out = mySSH.connect(ip=ip, settings_authen=settings_authen, isCISCO=isCISCO,send_msg=True)
    print(f'  [PID:{pid}] Open SSH connection to {ip}')

    if channel != 1 and channel != 2:
        for raw_cli in cli_list:
            try :
                cmd_out, sum_out, prompt = mySSH.send(raw_cli, channel, ip, sum_out, send_msg=True, recv_msg=False,
                                                  isCISCO=isCISCO)

            except ssh_exception.SSHException as e :
                print(f'Send command Error : {str(e)}')
                time.sleep(5)
                print(f'[PID:{pid}] Start process "NODE {ip}"')

                session, channel, temp_sum_out = mySSH.connect(ip=ip, settings_authen=settings_authen, isCISCO=isCISCO)
                print(f'  [PID:{pid}] Open SSH connection to {ip}')
                sum_out += temp_sum_out
                cmd_out, sum_out, prompt = mySSH.send(raw_cli, channel, ip, sum_out, send_msg=True, recv_msg=False,
                                              isCISCO=isCISCO)

        if isinstance(channel, Channel):
            channel.close()

        output_command = sum_out.replace('\r', '')
    elif channel == 1:
        result = f'[PID:{pid}] Stop process "{ip}" due to login error\n'
        print(result)
        output_command = result
        logging.error(result)
    elif channel == 2:
        result = f'[PID:{pid}] Stop process "{ip}" due to connection error\n'
        print(result)
        output_command = result
        logging.error(result)

    return output_command

def load_service(result_readlines,servicetype,serviceid):
    from re import search,MULTILINE
    from modules.Obj import objService_status,objSap_status,objSdp_status,objInterface_status
    sap_dict = dict()
    sdp_dict = dict()
    maclist = []
    sap_regex = 'sap:((?:\d+\/\d+(?:\/c\d)?\/\d+|lag-\d+)(?:\:\d+(?:[.](?:\d+|\*))?)?[ ]+)'
    sdp_regex = 'sdp:(\d+:\d+)'
    m1 = search(r'MINOR:[ ]CLI[ ]Invalid[ ]service[ ]id[ ]\"\d{9}\"', '\n'.join(result_readlines))
    if not m1:
        aobjservice_status = objService_status()
        aobjservice_status.serviceno = serviceid
        aobjservice_status.serviceType = servicetype
    else:
        aobjservice_status = None
        return aobjservice_status

    for line in result_readlines :

        m2 = search(r'Name[ ]{14}:[ ](.*)', line)
        m3 = search(r'Description[ ]{7}:[ ](.*)', line)
        m4 = search(r'Customer[ ]Id[ ]{7}:[ ](\d+)', line)
        m5 = search(r'SAP[ ]Count[ ]{9}:[ ](\d+)[ ]+SDP[ ]Bind[ ]Count[ ]+:[ ](\d+)',
                    line)  # g1 = SAP count, g2 = SDP count
        m6 = search(r'Admin[ ]State[ ]{7}:[ ](.*)[ ]+Oper[ ]State[ ]+:[ ](.*)',
                    line)  # group 1 = Admin State, group 2 = Oper State
        m7 = search(r'^MTU[ ]{15}:[ ](\d+)[ ]+', line,MULTILINE)
        m8 = search(
            r''+sap_regex+'(.*)[ ]+(\d+)[ ]+(\d+)[ ]+(.*)[ ]+(.*)',
            line)
        m9 = search(r'sdp:(\d+:\d+)[ ][S|M]\((.*)\)[ ]+([Spok|Mesh]+)[ ]+(\d+)[ ]+(\d+)[ ]+(.*)[ ]+(.*)', line)
        m11 = search(
            r'(\d+)[ ]+((?:[a-f0-9]{2}:){5}(?:[a-f0-9]{2}))[ ](' + sap_regex + '|'+sdp_regex+')[ ]+(.*)(\d{2}/\d{2}/\d{2}[ ]\d{2}:\d{2}:\d{2})',
            line)
        m_interface = search(r'show[ ]router[ ](\d+)[ ]interface[ ]\"(.*)\"', line)
        m_interface_state = search(r'(.*)[ ]+(Up|Down)[ ]+(Up|Down)/(Up|Down)[ ]+VPRN', line)

        if m2:
            svc_name = '-' if m2.group(1) == '(Not Specified)' else m2.group(1)
            aobjservice_status.servicename = svc_name
        elif m3:
            svc_desc = '-' if m3.group(1) == '(Not Specified)' else m3.group(1)
            aobjservice_status.description = svc_desc

        elif m4:
            customer_id = m4.group(1)
            aobjservice_status.customer = customer_id

        elif m5:
            sap_count = m5.group(1)
            sdp_count = m5.group(2)
            aobjservice_status.sapcount = sap_count
            aobjservice_status.sdpcount = sdp_count
        elif m6:
            admin_state = m6.group(1).rstrip()
            oper_state = m6.group(2)
            if str(admin_state).lower() == 'up' :
                admin_state = objService_status.up_state
            elif str(admin_state).lower() == 'down' :
                admin_state = objService_status.down_state
            if str(oper_state).lower() == 'up' :
                oper_state = objService_status.up_state
            elif str(oper_state).lower() == 'down' :
                oper_state = objService_status.down_state
            aobjservice_status.service_adminstate = admin_state
            aobjservice_status.service_operstate = oper_state
        elif m7 :
            mtu_size = m7.group(1)
            aobjservice_status.mtu = mtu_size
        elif m8:
            sap_id = m8.group(1).rstrip()
            if ':' in sap_id:
                sap_vlan = str(sap_id).split(':')[1]
                sap_port = str(sap_id).split(':')[0]
            else :
                sap_port = sap_id
                sap_vlan = None
            sap_type = m8.group(2).rstrip()
            sap_adm_mtu = m8.group(3)
            sap_oper_mtu = m8.group(4)
            sap_adm_state = m8.group(5).rstrip()
            sap_oper_state = m8.group(6).rstrip()
            aobjsap = objSap_status()
            aobjsap.fullsap = sap_id
            aobjsap.port = sap_port
            aobjsap.vlan = sap_vlan
            if str(sap_adm_state).lower() == 'up' :
                sap_adm_state = objSap_status.up_state
            elif str(sap_adm_state).lower() == 'down' :
                sap_adm_state = objSap_status.down_state
            if str(sap_oper_state).lower() == 'up' :
                sap_oper_state = objSap_status.up_state
            elif str(sap_oper_state).lower() == 'down' :
                sap_oper_state = objSap_status.down_state
            aobjsap.encaptype = sap_type
            aobjsap.sap_adminstate = sap_adm_state
            aobjsap.sap_operstate = sap_oper_state
            aobjsap.sap_admin_mtu = sap_adm_mtu
            aobjsap.sap_oper_mtu = sap_oper_mtu
            if sap_id not in sap_dict.keys():
                sap_dict[sap_id] = aobjsap
                aobjservice_status.sap.append(aobjsap)
        elif m9:
            sdp_id = m9.group(1)
            sdp_far_end = m9.group(2)
            sdp_type = 'Spoke' if m9.group(3) == 'Spok' else m9.group(3)
            sdp_adm_mtu = m9.group(4)
            sdp_oper_mtu = m9.group(5)
            sdp_adm_state = m9.group(6).rstrip()
            sdp_oper_state = m9.group(7)
            aobjsdp = objSdp_status()
            aobjsdp.sdptype = sdp_type
            aobjsdp.sdpno = str(sdp_id).split(':')[0]
            aobjsdp.vcid = str(sdp_id).split(':')[1]
            aobjsdp.farend = sdp_far_end
            if str(sdp_adm_state).lower() == 'up' :
                sdp_adm_state = objSdp_status.up_state
            elif str(sdp_adm_state).lower() == 'down' :
                sdp_adm_state = objSdp_status.down_state
            if str(sdp_oper_state).lower() == 'up' :
                sdp_oper_state = objSdp_status.up_state
            elif str(sdp_oper_state).lower() == 'down' :
                sdp_oper_state = objSdp_status.down_state
            aobjsdp.sdp_adminstate = sdp_adm_state
            aobjsdp.sdp_operstate = sdp_oper_state
            aobjsdp.sdp_admin_mtu = sdp_adm_mtu
            aobjsdp.sdp_oper_mtu = sdp_oper_mtu

            if sdp_id not in sdp_dict.keys():
                sdp_dict[sdp_id] = aobjsdp
                aobjservice_status.sdp.append(aobjsdp)

        elif m11 :
            mac_addr = m11.group(2)
            source_id = m11.group(3)
            source_type = 'SAP' if 'sap' in source_id else 'SDP'
            source_id = source_id.replace('sap:', '').replace('sdp:', '')
            mac_age = m11.group(6).replace('L/', '').rstrip()
            last_change = m11.group(7)

            local_mac = [mac_addr, source_type, source_id, mac_age, last_change]
            if local_mac not in maclist :
                maclist.append(local_mac)
        elif m_interface :
            vprn_int = m_interface.group(2)
            aobjinterface = objInterface_status(interfaceName=vprn_int)
        elif m_interface_state :
            admin_state = m_interface_state.group(2)
            oper_state = m_interface_state.group(3)
            if str(admin_state).lower() == 'up' :
                admin_state = objInterface_status.up_state
            elif str(admin_state).lower() == 'down' :
                admin_state = objInterface_status.down_state
            if str(oper_state).lower() == 'up' :
                oper_state = objInterface_status.up_state
            elif str(oper_state).lower() == 'down' :
                oper_state = objInterface_status.down_state
            aobjinterface.interface_oper_state = oper_state
            aobjinterface.interface_admin_state = admin_state
            if aobjinterface not in aobjservice_status.interface :
                aobjservice_status.interface.append(aobjinterface)

    aobjservice_status.maclist = maclist

    return aobjservice_status

def getservicedetail(filepath_per_service,serviceid):
    aobjservice = None
    with open(filepath_per_service,mode='r') as reader :
        result_readlines = reader.readlines()
    if 'epipe' in str(filepath_per_service).lower() :
        # part epipe
        servicetype = 'epipe'

        aobjservice = load_service(result_readlines=result_readlines,servicetype=servicetype,serviceid=serviceid)

    elif 'vpls' in str(filepath_per_service).lower() :
        servicetype = 'vpls'
        aobjservice = load_service(result_readlines=result_readlines,servicetype=servicetype,serviceid=serviceid)

    elif 'vprn' in str(filepath_per_service).lower() :
        servicetype = 'vprn'
        aobjservice = load_service(result_readlines=result_readlines,servicetype=servicetype,serviceid=serviceid)

    elif 'xconnect' in str(filepath_per_service).lower() :
        servicetype = 'xconnect'
    elif 'bridge' in str(filepath_per_service).lower() :
        servicetype = 'bridge'
    elif 'vrf' in str(filepath_per_service).lower() :
        servicetype = 'vrf'


    return aobjservice
