from cryptography.fernet import Fernet
from os.path import exists

def createKEY(folderauthen):
    # key generation

    key = Fernet.generate_key()
    filekey = folderauthen + 'filekey.key'
    if exists(filekey):
        return
    # string the key in a file
    with open(filekey, 'wb') as filekey:
        filekey.write(key)

def loadKEY(folderauthen):
    # opening the key
    with open(folderauthen + 'filekey.key', 'rb') as filekey:
        key = filekey.read()
    return key

def encrypt(filename,key) :
    f = Fernet(key)
    with open(filename, "rb") as file:
        # read all file data
        file_data = file.read()
    if b'node_usr' not in file_data :
        return
    else :
        encrypted_data = f.encrypt(file_data)
        print(encrypted_data)
        # write the encrypted file
        with open(filename, "wb") as file:
            file.write(encrypted_data)

def decrypt(filename, key):
    """
    Given a filename (str) and key (bytes), it decrypts the file and write it
    """
    f = Fernet(key)
    with open(filename, "rb") as file:
        # read the encrypted data
        encrypted_data = file.read()
    # decrypt data
    decrypted_data = f.decrypt(encrypted_data)
    return decrypted_data