

class objFolderInput(object):
    def __init__(self):
        self.prefix = None
        self.foldername = None
        self.filenodelist = []
        self.owner_folderoutput = None
        self.folder_perservice = '//per_service//'

class objSummary(object):
    def __init__(self):
        self.saplist = []
        self.startnode = None


class objservice_in_summary(object):
    def __init__(self):
        self.sapowner = None
        self.scenario = None
        self.nodecheck = dict()

class objScenario(object):
    bbhsi = 'BBHSI'
    wifihsi = 'WIFIHSI'
    cwlc = 'CWLC'
    cwlcv2 = 'CWLCV2'
    mgmt = 'MGMT'
    mgmtv2 = 'MGMTV2'
    voice = 'VOICE'
    voicev2 = 'VOICEV2'
    voicespec = 'VOICE-SPECIAL'
    voicedid = 'VOICE-DID'
    dwlcbng = 'DWLC-BNG'
    dwlcbras = 'DWLC-BRAS'
    corpl3 = 'CORPL3'
    corpl2 = 'CORPL2'
    mobile = 'MOBILE'
    mobile_bfkt = 'MOBILE-BFKT'
    notmatch = 'Not match'

class objService_status(object):
    """docstring for objService"""
    service_epipe = 'epipe'
    service_vpls = 'vpls'
    service_vprn = 'vprn'
    service_xconnect = 'xconnect'
    service_bridge = 'bridge'
    service_vrf = 'vrf'
    up_state = 'Up'
    down_state = 'Down'

    def __init__(self, ):
        self.serviceType = None
        self.isvcswitching = False
        self.issvcsaptype_any = False
        self.serviceno = None
        self.customer = None
        self.mtu = None
        self.description = None
        self.servicename = None
        self.sapcount = 0
        self.sdpcount = 0
        self.service_adminstate = None
        self.service_operstate = None
        self.maclist = []


        self.obj_service = None
        self.child = {}

        self.sap = []
        self.sdp = []
        self.policystatement = []
        self.policyhead = []
        self.location = None
        self.clli = None
        self.systemip = None
        self.interface = []
        self.vrfexport = None
        self.vrfimport = None
        self.vrftarget = None
        self.asno = None
        self.routedist = None
        self.bgp = []
        self.autobind = None
        self.staticrt = []
        self.aggregate = []
        self.vprn = []
        self.splithorizongroup = []
        self.stp = None
        self.fdbtablesize = None
        self.endpoint = None
        self.endpointName = None
        self.isShutdown = False
        self.allowipintbind = None
        self.autonomoussystem = None
        self.ecmp = None
        self.customerCfg= []
        self.unregcognize = None
        self.clli_onInf= None
        self.circuit = None
        self.agnlist = []
        self.oldserviceno = None
        self.isrvpls = False
        self.rvplsName = ""
        self.ospf = []
        self.rip = []
        self.ntp = None
        self.portdescription = ""
        self.origininterface_cisco = None
        self.msgerr = None
        self.isBB = False
        self.maxroute = None
        self.threshold = None
        self.iscisco = False
        self.ciscoid = None
        self.cisco_servicetype = None
        self.cisco_groupname = None
        self.cisco_servicename = None
        self.protocol_pece = ['direct','static']
        self.enablebgpvpnbackupipv4 = False
        self.structure_service = None
        self.iscpe = False
        self.isXE = False
        self.sdp_source = None
        self.vplsid = None
        self.interfaceName_ADA = []



    def setlocationAndClli(self,systeminfo_list):
        if systeminfo_list != None:
            self.location = systeminfo_list['location']
            self.clli = systeminfo_list['clli']
            self.systemip = systeminfo_list['systemip']
    def getTagheadeCfg(self):
        temp = []
        if self.location != None and self.location != '':
            temp.append(self.location)
        if self.clli != None and self.clli != '':
            temp.append(self.clli)
        if self.systemip != None and self.systemip != '':
            temp.append(self.systemip)
        return ','.join(temp)
    def getSystemIP(self):
        return self.systemip
    def getHeaderCfg(self):
        result = {'old':'','new':'','restore':''}
        new_header = "# {headeran} new config\n".format(headeran=self.getTagheadeCfg())
        restore_header = "# {headeran} restore config\n".format(headeran=self.getTagheadeCfg())
        old_header = "# {headeran} old config\n".format(headeran=self.getTagheadeCfg())
        result['old'] = old_header
        result['new'] = new_header
        result['restore'] = restore_header
        return result


class objSap_status(object):
    """docstring for bojSap"""
    bundle_str = "Bundle-Ether"
    up_state = 'Up'
    down_state = 'Down'
    def __init__(self, ):
        self.port = None
        self.vlan = None
        self.fullsap = None
        self.encaptype = None
        self.sap_adminstate = None
        self.sap_admin_mtu = None
        self.sap_operstate =None
        self.sap_oper_mtu =None

        self.splithorizongroup = None
        self.endpointName = None
        self.filtermac = None
        self.filterCfg = []
        self.isBundle = False
        self.lagcisco = None
        self.portCfg = None
        self.description = None
        self.ciscoport = None
        self.portlist = []
        self.seconddot = '0'
        self.isshutdown = False
        self.unregcognize = None
        self.isqinq = False
        self.prefixcisco = None
        self.portdescription = None
        self.lagpriority = None
        self.lagCfg = None
        self.isMCLAG = False
        self.is1G = False
        self.scenario = None
        self.stormcontrol = None
        self.hasSplit = False
        self.originport = None
        self.originnode = None
        self.opergroup = None
    def getSAP(self):
        if self.vlan is not None and not self.isqinq :
            if self.seconddot != '0' :
                self.vlan = self.seconddot+ "."+ self.vlan
                self.isqinq = True
            else :
                self.vlan = self.vlan+'.'+self.seconddot
                self.isqinq = True

        if self.isBundle :
            localport = self.bundle_str+self.port
        else :
            localport = self.port


        print(f'[getSAP] localport : {localport}')
        print(f'[getSAP] vlan : {self.vlan}')
        if self.vlan is not None :
            return  (localport + ':' +self.vlan)
        else :
            return self.port
class objInterface_status(object):
    """docstring for objInterface"""
    up_state = 'Up'
    down_state = 'Down'

    def __init__(self, interfaceName):
        self.interfaceName = interfaceName
        self.interface_admin_state = None
        self.interface_oper_state = None

        self.sdp = []
        self.sap = []
        self.originsap = []
        self.address = None
        self.secondary = []
        self.description = None
        self.vrrp = []
        self.vpls = None
        self.remoteproxyarp = None
        self.isShutdown = False
        self.mac = None
        self.interface_inused = False
        self.unregcognize = None
class objVrrp(object):
    """docstring for objVrrp"""
    def __init__(self, vrrpName,vrrp_str):
        self.vrrpName = vrrpName
        self.vrrp_str = vrrp_str
        self.backup = []
        self.priority = None
        self.initdelay = None
        self.messageinterval = None
        self.pingreply=False
        self.bfd_interval = None
        self.bfd_multiplier = None
        self.holdtimedown = None
        self.bfd_peerip = None
        self.bfd_serviceid = None
        self.bfd_interfacename = None
        self.standbyforwarding = False
        self.unregcognize = None

class objSdp_status(object):
    """docstring for objSAP"""
    protocol_ldp = 'ldp'
    protocol_rsvp = 'rsvp'
    protocol_srisis = 'sr-isis'
    protocol_bgplu = 'bgp-tunnel'
    up_state = 'Up'
    down_state = 'Down'
    def __init__(self,):
        self.sdp_str = None
        self.sdptype = None
        self.sdpno = None
        self.vcid = None
        self.farend = None
        self.sdp_adminstate = None
        self.sdp_admin_mtu = None
        self.sdp_operstate = None
        self.sdp_oper_mtu = None


        self.precedence = None
        self.isShutdown = False
        self.endpointName = None
        self.splithorizongroupName = None
        self.forcevlanvcforwarding = False
        self.vlanvctag = None
        self.sdpCfg = []
        self.sdpold = None
        self.newipdst = None
        self.isnewSDP = False
        self.isicb = False
        self.vctype = None
        self.protocol = None
        self.hashlabel = False
        self.signalcap = False
        self.unregcognize = None
        self.hasSplit = False
        self.monitorgroup = None
        self.filterCfg = []
        self.ingress = []

    def set_ipdst(self,ip):
        self.ipdst = ip
    def get_SDPcfg(self,description=None,space=8,isSpecialPE=False):
        sdpcfg = ''
        if description != None :
            sdpcfg =(' '*space)+ 'sdp '+str(self.sdpno) +' mpls create\n'
            sdpcfg +=(' '*space)+(' '*4) + 'description "'+description+'"\n'
            sdpcfg +=(' '*space)+(' '*4) + 'far-end '+str(self.ipdst)+'\n'
            sdpcfg +=(' '*space)+(' '*4) +str(self.protocol)+'\n'
            sdpcfg +=(' '*space)+(' '*4) + 'path-mtu 9194\n'
            sdpcfg +=(' '*space)+(' '*4) + 'keep-alive\n'
            sdpcfg +=(' '*space)+(' '*8) + 'shutdown\n'
            sdpcfg +=(' '*space)+(' '*4) + 'exit\n'
            sdpcfg +=(' '*space)+(' '*4) + 'no shutdown\n'
            sdpcfg +=(' '*space)+'exit\n'
        elif isSpecialPE :
            sdpcfg =(' '*space)+ 'sdp '+str(self.sdpno) +' mpls create\n'
            sdpcfg +=(' '*space)+(' '*4) + 'far-end '+str(self.ipdst)+'\n'
            sdpcfg +=(' '*space)+(' '*4) +str(self.protocol)+'\n'
            sdpcfg +=(' '*space)+(' '*4) + 'path-mtu 9194\n'
            sdpcfg +=(' '*space)+(' '*4) + 'keep-alive\n'
            sdpcfg +=(' '*space)+(' '*8) + 'shutdown\n'
            sdpcfg +=(' '*space)+(' '*4) + 'exit\n'
            sdpcfg +=(' '*space)+(' '*4) + 'no shutdown\n'
            sdpcfg +=(' '*space)+'exit\n'
        else :
            if self.sdpCfg :
                sdpcfg = self.sdpCfg[0].obj_service
            else :
                sdpcfg = 'echo "*****Please recheck SDP no. {sdpno}"*****\n'.format(sdpno=self.sdpno)
        return sdpcfg
    def get_opergroupcfg(self,space=8):
        opercfg = ''
        if self.monitorgroup is not None :
            opercfg = (' ' * space) + 'oper-group "' + str(self.monitorgroup) + '" create\n'
            opercfg +=(' '*space)+'exit\n'
        return opercfg


class settingpath(object):
    def __init__(self):
        self.folder_Authen = None
        self.fileauthen_cisco = None
        self.fileauthen_sam = None
        self.folderlog = None
        self.folderoutput = None
        self.folderinput = None

class objNodecmd(object):
    def __init__(self):
        self.nodeip = None
        self.commandlist = []
        self.vendor = None
        self.devicetype = None
        self.autheninfo = None
        self.objfolderinput = None
