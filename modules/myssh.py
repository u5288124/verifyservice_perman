import paramiko
import logging
import time
import re
import os
logging.basicConfig(filename='app.log')
logging.getLogger("paramiko").setLevel(logging.DEBUG)
regex_prompt_nokia = r'((?:\*)*[A|B]:(.*)#)'
regex_prompt_cisco_xr = r'(^RP\/.*:(.*)#)'
regex_prompt_cisco_xe = r'(^(.*)#)'

def connect(ip, settings_authen, delay1=0.2, delay2=0.5, timeout=20, send_msg=False, recv_msg=False,isCISCO=False):
    '''

    :param ip:
    :param settings_authen: dict => { sam_ip , sam_username, sam_password, node_username, node_password }
    :param delay1: firstdelay [default=0.2 sec]
    :param delay2: delay after send command [default=0.5 sec]
    :param timeout: [default=10 sec]
    :param send_msg:
    :param recv_msg:
    :param isCISCO:
    :return:
    '''
    pid = os.getpid()
    sum_out = ''
    nodeConnectionError = False
    nodeLoginError = False
    prompt = None
    session = paramiko.SSHClient()
    session.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    local_user = settings_authen["node_usr"].encode('ascii')
    local_pass = settings_authen['node_pass'].encode('ascii')
    session.connect(settings_authen['sam_bbl_ip'], username=settings_authen['sam_bb1_usr'], password=settings_authen['sam_bb1_pass'])

    channel = session.invoke_shell()
    print(f'  [PID:{pid}] Open SSH connection to {ip}')
    while True:
        time.sleep(delay1)
        if channel.recv_ready():
            sum_out += channel.recv(65535).decode('ascii')

        if 'Enter IP address [press q/Q to quit]:' in sum_out:
            if recv_msg is True:
                print(f'  [PID:{pid}] Receive "Enter IP address [press q/Q to quit]:" from {ip}')
            if send_msg is True:
                print(f'  [PID:{pid}] Send "{ip}" to {settings_authen["sam_bbl_ip"]}')


            channel.send(ip.encode('ascii') + b'\n')
            time.sleep(delay2)
            start_connection = time.time()   # เริ่มนับเวลาหลังจากใส่ IP
            break
    while True:
        time.sleep(delay1)
        if time.time() - start_connection > timeout:  # ถ้าใส่ IP ไปมากกว่า 10 วินาที แปลว่า connection มีปัญหา
            nodeConnectionError = True
            status = f'  [PID:{pid}] Error! Can\'t connect to {ip}, please check your network or VPN'
            print(status)
            break
        if channel.recv_ready():
            sum_out += channel.recv(65535).decode('ascii')
        # FOR LAB
        if '[root@dhost ~]#' in sum_out :
        # if 'Login:' in sum_out or 'Username:' in sum_out:
            if isCISCO:
                local_user = settings_authen["node_usr"]
            else:
                local_user = settings_authen["nokia_user"]

            if recv_msg is True:
                print(f'  [PID:{pid}] Receive "Login:" from {ip}')
            if send_msg is True:
                print(f'  [PID:{pid}] Send "{local_user}" to {ip}')

            # TEMP SSH in LAB
            temp_lab = b'ssh ' + local_user.encode('ascii') + b'@'
            channel.send(temp_lab + ip.encode('ascii') + b'\n')

            # channel.send(local_user.encode('ascii') + b'\n')
            break

    if nodeConnectionError is False:    # ถ้า connection ไม่มีปัญหาค่อยทำต่อไป
        while True:

            time.sleep(delay1)
            if time.time() - start_connection > timeout:  # ถ้าใส่ IP ไปมากกว่า 10 วินาที แปลว่า connection มีปัญหา
                print(f'  [PID:{pid}] Open SSH connection : channel => 2 ')
                return 2, None  # Channel = 2 คือ connect node ไม่ได้
            if channel.recv_ready():
                sum_out += channel.recv(65535).decode('ascii')

            # FOR LAB
            if 'password:' in sum_out:
            # if 'Password:' in sum_out:
                if recv_msg is True:
                    print(f'  [PID:{pid}] Receive "Password:" from {ip}')
                if send_msg is True:
                    print(f'  [PID:{pid}] Send "**********" to {ip}')
                if isCISCO :
                    local_pass = settings_authen['node_pass'].encode('ascii')
                else :
                    local_pass = settings_authen['nokia_pass'].encode('ascii')

                channel.send(local_pass + b'\n')
                time.sleep(delay2)
                start_login = time.time()  # เริ่มนับเวลาหลังจากใส่ Password
                break
        cmd_out = ''
        while True:
            time.sleep(delay1)
            if time.time() - start_login > timeout:  # ถ้าใส่ password ไปมากกว่า 10 วินาที แปลว่า username/password มีปัญหา
                nodeLoginError = True
                status = f'  [PID:{pid}] Error! Can\'t login to {ip}, please check username or password'
                print(status)
                break
            if channel.recv_ready():
                data = channel.recv(65535).decode('ascii')
                sum_out += data
                cmd_out += data
            if not isCISCO :
                PROMPT = re.search(regex_prompt_nokia, cmd_out,re.MULTILINE)
            else :
                PROMPT = re.search(regex_prompt_cisco_xr, cmd_out,re.MULTILINE)
                if not PROMPT:
                    PROMPT = re.search(regex_prompt_cisco_xe, cmd_out,re.MULTILINE)
            if PROMPT:
                prompt = PROMPT.group(1)
                print(f'LOGIN SUCCESS')
                break
            else :
                print(f'Not found prompt.')
            if 'Press any key to continue' in data:
                channel.send(b'q\n')
                print(f'LOGIN SUCCESS BUT FOUND NODE BUG')
                break
        if nodeLoginError is False:  # ถ้า connection ไม่มีปัญหาค่อยทำต่อไป
            return channel, sum_out
        else:
            print(f'  [PID:{pid}] Open SSH connection : channel => 1 ')

            return 1, None    # Channel = 1 คือ login node ไม่ได้
    else:
        print(f'  [PID:{pid}] Open SSH connection : channel => 2 ')
        return 2, None        # Channel = 2 คือ connect node ไม่ได้


def send(command, channel, ip, sum_out, delay1=0.2, send_msg=True, recv_msg=True,isCISCO=False):
    pid = os.getpid()
    cmd_out = ''  # output ของ command นั้นๆ
    prompt = ''
    t1 = time.time()
    if send_msg is True:
        print(f'  [PID:{pid}] Send "{command}" to {ip}')
    channel.send(command.encode('ascii') + b'\n')
    while True:
        time.sleep(delay1)
        if channel.recv_ready():
            data = channel.recv(65535).decode('ascii')

            sum_out += data
            cmd_out += data
            if command == 'logout' or command == 'exit' :
                # FOR LAB
                if '[root@dhost ~]#' in data:
                    break
                elif 'exit' in data :
                    break

                # if 'Enter IP address [press q/Q to quit]:' in data:
                #     break


            elif command == 'q':
                if 'Goodbye' in data:
                    break
            else:

                if not isCISCO :
                    PROMPT = re.search(regex_prompt_nokia, cmd_out,re.MULTILINE)
                else :
                    PROMPT = re.search(regex_prompt_cisco_xr, cmd_out,re.MULTILINE)
                    if not PROMPT :
                        PROMPT = re.search(regex_prompt_cisco_xe, cmd_out,re.MULTILINE)
                if PROMPT:
                    prompt = PROMPT.group(1)

                    if recv_msg is True:
                        print(f'  [PID:{pid}] Receive "{command}" output from {ip} [Wait={round(time.time() - t1, 2)}]')
                    break
    cmd_out = cmd_out.replace('\r', '')
    return cmd_out, sum_out, prompt


if __name__ == '__main__':
    logging.basicConfig()
    logging.getLogger("paramiko").setLevel(logging.NOTSET)  # for example
    setting_authen = {'node_usr': 'MonMue.ada@locs', 'node_pass': 'HomeHoldAway64=', 'sam_bbl_ip': '10.50.64.181', 'sam_bb1_usr': 'nokia', 'sam_bb1_pass': 'Pwd@1234', 'nokia_user': 'NutAng.nok@locs', 'nokia_pass': 'GrayFillDuck38&'}
    ip = '10.167.51.1'
    iscisco = False
    channel, sum_out = connect(ip=ip,settings_authen=setting_authen,send_msg=True,isCISCO=iscisco)
    print(f'done.')
    if isinstance(channel,paramiko.Channel):
        cmd_out, sum_out, prompt = send('logout', channel, ip, sum_out, send_msg=True, recv_msg=False,isCISCO=iscisco)
        cmd_out, sum_out, prompt = send('q', channel, ip, sum_out, send_msg=True, recv_msg=False,isCISCO=iscisco)
        print(sum_out)
        print(f'promt : {prompt}')
        channel.close()