def split_log_nokia(objfolderinput):
    from os import listdir,mkdir
    from os.path import exists
    from re import finditer,MULTILINE
    from modules.mylib import loadSettingPath
    outputpath = loadSettingPath().folderoutput
    log_path = outputpath + objfolderinput.foldername
    files = listdir(log_path)
    folder_seperate_service = log_path + objfolderinput.folder_perservice
    if not exists(folder_seperate_service):
        mkdir(folder_seperate_service)
    for output in files:
        if '_All.txt' in output :
            ip = output.rstrip('_All.txt')
            split_log = open(log_path + output).read()
            match_regex = finditer(
                r'((?:\**)[AB]:(?:.*)#[-]{13}[ ]START SERVICE[ ](epipe|vpls|vprn)[ ](.*)[ ][-]{13}#[\s\S]*?#[-]{13}[ ]END SERVICE[ ](?:epipe|vpls|vprn)[ ](?:.*)[ ][-]{13}#)',
                split_log, MULTILINE)
            for log in match_regex:
                srv_log = log.group(1)
                srv_type = log.group(2)
                srv_name = log.group(3)
                srv_name = srv_name.replace(' ','-')
                filename =  folder_seperate_service+ip + '_' + srv_type + '_' + srv_name + '.txt'
                with open(filename, 'w') as wr_split_log :
                    wr_split_log.write(srv_log)

def style(ws, cell_range):
    from openpyxl.styles import Font, Alignment, PatternFill, Border, Side
    first_row = list(ws.rows)[0]
    for cell in first_row:
        cell.font = Font(bold=True)
        cell.fill = PatternFill('solid', fgColor='DDDDDD')
    rows = ws[cell_range]
    for row in rows:
        for cell in row:
            cell.alignment = Alignment(horizontal='center', vertical='center')
            cell.border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
            if cell.value == 'N/A' or cell.value == '-' :
                cell.fill = PatternFill('solid', fgColor='bfbfbf')
            elif cell.value == 'Up' or cell.value == 'Success':
                cell.fill = PatternFill('solid', fgColor='c6efce')
                cell.font = Font(color='006100')
            elif cell.value == 'Down' or cell.value == 'Fail':
                cell.fill = PatternFill('solid', fgColor='ffc7ce')
                cell.font = Font(color='9c0006')

def write_sheet(ws, data_list):
    from openpyxl.utils import get_column_letter
    col_count = 0
    row_count = 0
    for row in data_list:
        row_count += 1
        ws.append(row)
        if row_count == 1:
            col_count = len(row)
    max_col_letter = get_column_letter(ws.max_column)
    max_row_number = str(len(ws['A']))
    header_range = f'A1:{max_col_letter}{ws.max_column}'
    value_range = f'A1:{max_col_letter}{max_row_number}'
    ws.auto_filter.ref = header_range
    excel_autowidth(ws)
    style(ws, value_range)

def excel_autowidth(worksheet) :
    from openpyxl.utils import get_column_letter

    for col in worksheet.columns:
        max_length = 0
        column = col[0].column # Get the column name
        for cell in col:
            try: # Necessary to avoid error on empty cells
                # print(cell.value , cell.value == None)
                # if cell.value != None:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except :
                pass
        adjusted_width = (int(max_length) + 2) * 1.2
        # worksheet.column_dimensions[column].width = int(adjusted_width)

        columnindex =  str(get_column_letter(column))

        worksheet.column_dimensions[columnindex].width = int(adjusted_width)

def toExcel(objfolderinput):
    from modules.Obj import objFolderInput
    from modules.mylib import loadSettingPath
    from os import listdir,getpid
    from re import search
    from openpyxl import Workbook
    from netaddr import IPNetwork
    pid = getpid()

    if isinstance(objfolderinput,objFolderInput):
        output_save = loadSettingPath().folderoutput
        path_perservice = f'{output_save}/{objfolderinput.foldername}/{objfolderinput.folder_perservice}'
        fileexcel_name = f'{output_save}/{objfolderinput.foldername}/@{str(objfolderinput.foldername).replace("/","")}'

        ownerNode = objfolderinput.foldername
        epipe_files = []
        vpls_files = []
        vprn_files = []
        pece_info = []
        macsap_dict = dict()
        GWMAC = dict()

        for file in listdir(path_perservice):  # วนลูปเอาชื่อไฟล์ทั้งหมดออกมา แต่จะมากรองด้วย pattern ด้านล่าง

            if search(r'(pre|post)_(.*)_(epipe|vpls|vprn)_(.*)[.]txt', file):  # 507850230_epipe_10.9.73.19
                split = file.split('_')  # split ชื่อไฟล์ออกมาด้วย '_'
                svc_type = split[2]
                if svc_type == 'epipe':
                    epipe_files.append(file)
                elif svc_type == 'vpls':
                    vpls_files.append(file)
                elif svc_type == 'vprn':
                    vprn_files.append(file)

        if epipe_files:
            output_file = 'epipe'
            service_list = [['Node', 'Service ID', 'Service Name', 'Description', 'Customer ID', 'SAP Count', 'SDP Count', 'Admin State', 'Oper State', 'MTU']]
            sap_list = [['Node', 'Service ID', 'Port', 'SAP ID', 'SAP Type', 'Admin MTU', 'Oper MTU', 'Admin State', 'Oper State']]
            sdp_list = [['Node', 'Service ID', 'SDP ID', 'Far End', 'SDP Type', 'Admin MTU', 'Oper MTU', 'Admin State', 'Oper State']]

            print(f'  [PID:{pid}] Summarize Epipe services for "{ownerNode}"')
            for file in epipe_files:
                split = file.split('_')  # split ชื่อไฟล์ออกมาด้วย '_'
                node_ip = split[1]  # Node IP
                svc_id = split[3].replace('.txt', '')  # Service ID
                sap_port = sap_id = sap_type = sap_adm_mtu = sap_oper_mtu = sap_adm_state = sap_oper_state = sdp_id = sdp_far_end \
                    = sdp_type = sdp_adm_mtu = sdp_oper_mtu = sdp_adm_state = sdp_oper_state = svc_name = svc_desc = customer_id \
                    = sap_count = sdp_count = admin_state = oper_state = mtu_size = None
                with open(f'{path_perservice}/{file}') as f:  # เปิดอ่าน output จากไหนไฟล์
                    NoServiceFound = False
                    mtu_size = None  # ทำให้ใช้ค่า MTU ที่เจอครั้งแรกเท่านั้น (RegEx มันจะไป match กับ LMTU ด้วย)
                    for line in f.readlines():
                        m1 = search(r'MINOR:[ ]CLI[ ]Invalid[ ]service[ ]id[ ]\"\d{9}\"', line)
                        m2 = search(r'Name[ ]{14}:[ ](.*)', line)
                        m3 = search(r'Description[ ]{7}:[ ](.*)', line)
                        m4 = search(r'Customer[ ]Id[ ]{7}:[ ](\d+)', line)
                        m5 = search(r'SAP[ ]Count[ ]{9}:[ ](\d+)[ ]+SDP[ ]Bind[ ]Count[ ]+:[ ](\d+)', line)  # g1 = SAP count, g2 = SDP count
                        m6 = search(r'Admin[ ]State[ ]{7}:[ ](.*)[ ]+Oper[ ]State[ ]+:[ ](.*)', line)  # group 1 = Admin State, group 2 = Oper State
                        m7 = search(r'MTU[ ]{15}:[ ](\d+)[ ]+', line)
                        m8 = search(r'sap:((\d+\/\d+(?:\/c\d)?\/\d+|lag-\d+):\d+([.](\d+|\*)|)[ ]+)(.*)[ ]+(\d+)[ ]+(\d+)[ ]+(.*)[ ]+(.*)', line)
                        m9 = search(r'sdp:(\d+:\d+)[ ][S|M]\((.*)\)[ ]+([Spok|Mesh]+)[ ]+(\d+)[ ]+(\d+)[ ]+(.*)[ ]+(.*)', line)
                        if m1:
                            NoServiceFound = True
                        elif m2:
                            svc_name = '-' if m2.group(1) == '(Not Specified)' else m2.group(1)
                        elif m3:
                            svc_desc = '-' if m3.group(1) == '(Not Specified)' else m3.group(1)
                        elif m4:
                            customer_id = m4.group(1)
                        elif m5:
                            sap_count = m5.group(1)
                            sdp_count = m5.group(2)
                        elif m6:
                            admin_state = m6.group(1).rstrip()
                            oper_state = m6.group(2)
                        elif m7 and mtu_size is None:
                            mtu_size = m7.group(1)
                        elif m8:
                            sap_id = m8.group(1).rstrip()
                            sap_port = m8.group(2)
                            sap_type = m8.group(5).rstrip()
                            sap_adm_mtu = m8.group(6)
                            sap_oper_mtu = m8.group(7)
                            sap_adm_state = m8.group(8).rstrip()
                            sap_oper_state = m8.group(9).rstrip()
                            sap_list.append([node_ip, svc_id, sap_port, sap_id, sap_type, sap_adm_mtu, sap_oper_mtu, sap_adm_state, sap_oper_state])
                        elif m9:
                            sdp_id = m9.group(1)
                            sdp_far_end = m9.group(2)
                            sdp_type = 'Spoke' if m9.group(3) == 'Spok' else m9.group(3)
                            sdp_adm_mtu = m9.group(4)
                            sdp_oper_mtu = m9.group(5)
                            sdp_adm_state = m9.group(6).rstrip()
                            sdp_oper_state = m9.group(7)
                            sdp_list.append([node_ip, svc_id, sdp_id, sdp_far_end, sdp_type, sdp_adm_mtu, sdp_oper_mtu, sdp_adm_state, sdp_oper_state])

                if NoServiceFound is False:     # ต้องไม่เจอคำว่า MINOR: CLI Invalid service id "xxxxxxxxx" ใน file
                    values = [node_ip, svc_id, svc_name, svc_desc, customer_id, sap_count, sdp_count, admin_state, oper_state, mtu_size]
                    # print(f'    - {values}')
                    service_list.append(values)
                else:
                    # print(f'    - [{node_ip}, {svc_id}, "NOT FOUND SERVICE"]')
                    service_list.append([node_ip, svc_id, 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'])

            wb = Workbook()
            ws1 = wb.active
            ws1.title = "Service"
            write_sheet(ws1, service_list)
            print(f'    - [PID:{pid}] Created "Epipe service" worksheet for "{ownerNode}"')

            ws2 = wb.create_sheet('SAP')
            write_sheet(ws2, sap_list)
            print(f'    - [PID:{pid}] Created "Epipe SAP" worksheet for "{ownerNode}"')

            ws3 = wb.create_sheet('SDP')
            write_sheet(ws3, sdp_list)
            print(f'    - [PID:{pid}] Created "Epipe SDP" worksheet for "{ownerNode}"')

            print(f'    - [PID:{pid}] Saved file to "{output_save}/{objfolderinput.foldername}/@{objfolderinput.foldername}_{output_file}.xlsx"')
            wb.save(f'{fileexcel_name}_{output_file}.xlsx')

        if vpls_files:
            moved_sap_list = []
            output_file = 'vpls'
            service_list = [['Node', 'Service ID', 'Service Name', 'Description', 'Customer ID', 'SAP Count', 'SDP Count', 'Admin State', 'Oper State', 'MTU']]
            sap_list = [['Node', 'Service ID', 'Move?', 'Port', 'SAP ID', 'SAP Type', 'Admin MTU', 'Oper MTU', 'Admin State', 'Oper State']]
            sdp_list = [['Node', 'Service ID', 'SDP ID', 'Far End', 'SDP Type', 'Admin MTU', 'Oper MTU', 'Admin State', 'Oper State']]
            mac_list = [['Node', 'Service ID', 'MAC', 'Source Type', 'Source', 'Age', 'Last Change']]
            sap_regex = 'sap:((?:\d+\/\d+(?:\/c\d)?\/\d+|lag-\d+)(?:\:\d+(?:[.](?:\d+|\*))?)?[ ]+)'
            sdp_regex = 'sdp:(\d+:\d+)'
            print(f'  [PID:{pid}] Summarize VPLS services for "{ownerNode}"')
            for file in vpls_files:
                split = file.split('_')  # split ชื่อไฟล์ออกมาด้วย '_'
                node_ip = split[1]  # Node IP
                svc_id = split[3].replace('.txt', '')  # Service ID

                sap_move_status = sap_port = sap_id = sap_type = sap_adm_mtu = sap_oper_mtu = sap_adm_state = sap_oper_state \
                    = sdp_id = sdp_far_end = sdp_type = sdp_adm_mtu = sdp_oper_mtu = sdp_adm_state = sdp_oper_state = mac_addr \
                    = source_type = source_id = mac_age = last_change = svc_name = svc_desc = customer_id = sap_count \
                    = sdp_count = admin_state = oper_state = mtu_size = None

                with open(f'{path_perservice}/{file}') as f:  # เปิดอ่าน output จากไหนไฟล์
                    NoServiceFound = False
                    mtu_size = None  # ทำให้ใช้ค่า MTU ที่เจอครั้งแรกเท่านั้น (RegEx มันจะไป match กับ LMTU ด้วย)
                    for line in f.readlines():
                        m1 = search(r'MINOR:[ ]CLI[ ]Invalid[ ]service[ ]id[ ]\"\d{9}\"', line)
                        m2 = search(r'#[ ]SAP[:][ ](.*)', line)  # อ่านค่าบรรทัดแรกสุด ไว้ใช้ filter เอา SAP เฉพาะที่เราทำ
                        m3 = search(r'Name[ ]{14}:[ ](.*)', line)
                        m4 = search(r'Description[ ]{7}:[ ](.*)', line)
                        m5 = search(r'Customer[ ]Id[ ]{7}:[ ](\d+)', line)
                        m6 = search(r'SAP[ ]Count[ ]{9}:[ ](\d+)[ ]+SDP[ ]Bind[ ]Count[ ]+:[ ](\d+)', line)  # g1 = SAP count, g2 = SDP count
                        m7 = search(r'Admin[ ]State[ ]{7}:[ ](.*)[ ]+Oper[ ]State[ ]+:[ ](.*)', line)  # group 1 = Admin State, group 2 = Oper State
                        m8 = search(r'MTU[ ]{15}:[ ](\d+)[ ]+', line)
                        m9 = search(r'sap:((\d+\/\d+(?:\/c\d)?\/\d+|lag-\d+)(:\d+([.](?:\d+|\*))?|)?[ ]+)(.*)[ ]+(\d+)[ ]+(\d+)[ ]+(.*)[ ]+(.*)', line)
                        m10 = search(r'sdp:(\d+:\d+)[ ][S|M]\((.*)\)[ ]+([Spok|Mesh]+)[ ]+(\d+)[ ]+(\d+)[ ]+(.*)[ ]+(.*)', line)
                        m11 = search(
                            r'(\d+)[ ]+((?:[a-f0-9]{2}:){5}(?:[a-f0-9]{2}))[ ](' + sap_regex + '|' + sdp_regex + ')[ ]+(.*)(\d{2}/\d{2}/\d{2}[ ]\d{2}:\d{2}:\d{2})',
                            line)
                        if m1:
                            NoServiceFound = True
                        elif m2:
                            moved_sap_list = m2.group(1).split(';') if m2.group(1) != 'n/a' else []   # ถ้า match กับ m2 และไม่ใช่ค่า n/a (มี SAP อยู่จริง) # List ของ SAP ที่ย้ายโดยเอาไว้เทียบ
                        elif m3:
                            svc_name = '-' if m3.group(1) == '(Not Specified)' else m3.group(1)
                        elif m4:
                            svc_desc = '-' if m4.group(1) == '(Not Specified)' else m4.group(1)
                        elif m5:
                            customer_id = m5.group(1)
                        elif m6:
                            sap_count = m6.group(1)
                            sdp_count = m6.group(2)
                        elif m7:
                            admin_state = m7.group(1).rstrip()
                            oper_state = m7.group(2)
                        elif m8 and mtu_size is None:
                            mtu_size = m8.group(1)
                        elif m9:
                            sap_id = m9.group(1).rstrip()
                            sap_port = m9.group(2)
                            sap_type = m9.group(5).rstrip()
                            sap_adm_mtu = m9.group(6)
                            sap_oper_mtu = m9.group(7)
                            sap_adm_state = m9.group(8).rstrip()
                            sap_oper_state = m9.group(9).rstrip()
                            sap_move_status = 'Yes' if sap_id in moved_sap_list else '-'     # ถ้าเป็น SAP ที่ถูกย้าย
                            sap_list.append([node_ip, svc_id, sap_move_status, sap_port, sap_id, sap_type, sap_adm_mtu, sap_oper_mtu, sap_adm_state, sap_oper_state])
                        elif m10:
                            sdp_id = m10.group(1)
                            sdp_far_end = m10.group(2)
                            sdp_type = m10.group(3)
                            sdp_type = 'Spoke' if sdp_type == 'Spok' else sdp_type
                            sdp_adm_mtu = m10.group(4)
                            sdp_oper_mtu = m10.group(5)
                            sdp_adm_state = m10.group(6).rstrip()
                            sdp_oper_state = m10.group(7)
                            sdp_list.append([node_ip, svc_id, sdp_id, sdp_far_end, sdp_type, sdp_adm_mtu, sdp_oper_mtu, sdp_adm_state, sdp_oper_state])
                        elif m11:
                            mac_addr = m11.group(2)
                            source_id = m11.group(3)
                            source_type = 'SAP' if 'sap' in source_id else 'SDP'
                            source_id = source_id.replace('sap:', '').replace('sdp:', '')
                            mac_age = m11.group(6).replace('L/', '').rstrip()
                            last_change = m11.group(7)
                            if 'SAP' == source_type :
                                if node_ip not in macsap_dict.keys() :
                                    macsap_dict[node_ip] = dict()

                                if source_id not in macsap_dict[node_ip].keys():
                                    macsap_dict[node_ip][source_id] = {'mac':[]}
                                if mac_addr not in macsap_dict[node_ip][source_id]['mac']:
                                    macsap_dict[node_ip][source_id]['mac'].append(mac_addr)
                            local_mac = [node_ip, svc_id, mac_addr, source_type, source_id, mac_age, last_change]
                            if local_mac not in mac_list :
                                mac_list.append(local_mac)

                if NoServiceFound is False:     # ต้องไม่เจอคำว่า MINOR: CLI Invalid service id "xxxxxxxxx" ใน file
                    # print(f'    - {[svc_id, node_ip, svc_name, svc_desc, customer_id, sap_count, sdp_count, admin_state, oper_state, mtu_size]}')
                    service_list.append([node_ip, svc_id, svc_name, svc_desc, customer_id, sap_count, sdp_count, admin_state, oper_state, mtu_size])
                else:
                    # print(f'    - [{svc_id}, {node_ip}, \'NOT FOUND SERVICE\']')
                    service_list.append([node_ip, svc_id, 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'])

            wb = Workbook()
            ws1 = wb.active
            ws1.title = "Service"
            write_sheet(ws1, service_list)
            print(f'    - [PID:{pid}] Created worksheet for "VPLS service" for "{ownerNode}"')

            ws2 = wb.create_sheet('SAP')
            write_sheet(ws2, sap_list)
            print(f'    - [PID:{pid}] Created "VPLS SAP" worksheet for "{ownerNode}"')

            ws3 = wb.create_sheet('SDP')
            write_sheet(ws3, sdp_list)
            print(f'    - [PID:{pid}] Created "VPLS SDP" worksheet for "{ownerNode}"')

            ws4 = wb.create_sheet('FDB (related with cutover)')
            write_sheet(ws4, mac_list)
            print(f'    - [PID:{pid}] Created "VPLS FDB" worksheet for "{ownerNode}"')

            print(f'    - [PID:{pid}] Saved file to "{output_save}/{objfolderinput.foldername}/@{objfolderinput.foldername}_{output_file}.xlsx"')
            wb.save(f'{fileexcel_name}_{output_file}.xlsx')

        if vprn_files:
            output_file = 'vprn'
            interface_list = [['Node', 'Service ID', 'Interface', 'Address', 'Network ID', 'Admin State', 'Oper State', 'Peer IP', 'Ping Result']]
            arp_list = [['Node', 'Service ID', 'Interface', 'IP', 'MAC', 'Expire', 'Type']]
            vpnv4_routes = [['Node', 'Service ID', 'Neighbor', 'Type', 'Flag', 'Prefix', 'RD:RT', 'Next-hop']]
            print(f'  [PID:{pid}] Summarize VPRN services for "{ownerNode}"')
            for file in vprn_files:
                split = file.split('_')  # split ชื่อไฟล์ออกมาด้วย '_'
                node_ip = split[1]  # Node IP
                svc_id = split[3].replace('.txt', '')  # Service ID

                FoundShowVPNv4Command = False
                vprn_int = local_addr = admin_state = oper_state = peer_ip = ping_result = arp_int = arp_ip = arp_mac = arp_expire = arp_type = \
                    bgp_neighbor = route_type = flag = route_prefix = rd_rt = None
                with open(f'{path_perservice}/{file}') as f:  # เปิดอ่าน output จากไหนไฟล์
                    for line in f.readlines():
                        m1 = search(r'ping[ ]router[ ](\d+)[ ]((\d+[.]){3}\d+)[ ]source[ ]((\d+[.]){3}\d+)[ ]rapid', line)
                        m2 = search(r'5[ ]packets[ ]transmitted[,][ ](.*)(\d)[ ]packets[ ]received', line)
                        m3 = search(r'show[ ]router[ ](\d+)[ ]interface[ ]\"(.*)\"', line)
                        m4 = search(r'(.*)[ ]+(Up|Down)[ ]+(Up|Down)/(Up|Down)[ ]+VPRN', line)
                        m5 = search(r'[ ]{3}(((\d+[.]){3}\d+)/(\d{1,2})|-)[ ]{3}', line)
                        m6 = search(r'((\d+[.]){3}\d+)[ ]+(([a-f0-9]{2}:){5}[a-f0-9]{2}|[ ]+)[ ](\d{2}h\d{2}m\d{2}s)[ ](Oth\[I\]|Dyn\[I\]|Oth|Dyn)[ ]+(.*)', line)
                        m7_1 = search(r'show[ ]router[ ]bgp[ ]neighbor[ ](.*)[ ]vpn[-]ipv4[ ](received|advertised)[-]routes', line)
                        m7_2 = search(r'(.*)[ ]+((?:\d+)[:](?:\d+))[:]((?:.*)/(?:\d+))', line)
                        m7_3 = search(r'[ ]{6}(.*)[ ]+None[ ]{8}(?:\d+)', line)
                        if m1:
                            peer_ip = m1.group(2)
                            vprnno = m1.group(1)

                        if m2:
                            packet_received = m2.group(2)
                            if int(packet_received) < 4:     # 0-3
                                ping_result = 'Fail'
                            elif int(packet_received) >= 4:
                                ping_result = 'Success'
                        if m3:
                            vprn_int = m3.group(2)
                        if m4:
                            admin_state = m4.group(2)
                            oper_state = m4.group(3)
                        if m5:
                            int_ip2 = m5.group(1)
                            local_addr = f'{int_ip2}'
                            if local_addr == '-':
                                peer_ip = '-'
                                ping_result = '-'
                                network_id = '-'
                            else:
                                split = local_addr.split('/')
                                addr = IPNetwork(local_addr)
                                network_id = f'{str(addr.network)}/{split[1]}'
                            # print(f'    - {[node_ip, svc_id, vprn_int, local_addr, admin_state, oper_state, peer_ip, ping_result]}')
                            interface_list.append([node_ip, svc_id, vprn_int, local_addr, network_id, admin_state, oper_state, peer_ip, ping_result])
                        if m6:
                            arp_ip = m6.group(1)
                            arp_mac = m6.group(3).replace(' ', '')
                            if arp_mac == '':
                                arp_mac = '-'
                            if arp_mac != '-' :
                                if node_ip not in GWMAC.keys():
                                    GWMAC[node_ip] = dict()
                                if arp_mac not in GWMAC[node_ip].keys():
                                    GWMAC[node_ip][arp_mac] = {'vprnno': vprnno,'ipce':arp_ip,'ipGW':node_ip,'iscisco':False}
                            arp_expire = m6.group(5)
                            arp_type = m6.group(6)
                            arp_int = m6.group(7)
                            arp_list.append([node_ip, svc_id, arp_int, arp_ip, arp_mac, arp_expire, arp_type])
                        if m7_1:
                            FoundShowVPNv4Command = True
                            bgp_neighbor = m7_1.group(1)
                            route_type = m7_1.group(2).capitalize()
                        if FoundShowVPNv4Command is True:
                            if m7_2:
                                flag = m7_2.group(1).rstrip()
                                rd_rt = m7_2.group(2)
                                route_prefix = m7_2.group(3)
                                vpnv4_route = [node_ip, svc_id, bgp_neighbor, route_type, flag, route_prefix, rd_rt, None]
                            if m7_3 and vpnv4_route:
                                vpnv4_route[-1] = m7_3.group(1).rstrip()
                                vpnv4_routes.append(vpnv4_route)



            wb = Workbook()
            ws1 = wb.active
            ws1.title = "Interface"
            write_sheet(ws1, interface_list)
            print(f'    - [PID:{pid}] Created "VPRN Interface" worksheet for "{ownerNode}"')

            ws2 = wb.create_sheet('ARP Table')
            write_sheet(ws2, arp_list)
            print(f'    - [PID:{pid}] Created "VPRN ARP table" worksheet for "{ownerNode}"')

            ws3 = wb.create_sheet('VPNv4 Route')
            write_sheet(ws3, vpnv4_routes)
            print(f'    - [PID:{pid}] Created "VPRN VPNv4 Route" worksheet for "{ownerNode}"')

            print(f'    - [PID:{pid}] Saved file to "{output_save}/{objfolderinput.foldername}/@{objfolderinput.foldername}_{output_file}.xlsx"')
            wb.save(f'{fileexcel_name}_{output_file}.xlsx')

        return pece_info,macsap_dict,GWMAC