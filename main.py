program_version = '1.0'




def prompt_startprogram():
    print('===========================================================')
    print(f'Script verify services for TRUE IP Transport for 5G ver {program_version}')
    print('===========================================================')

def writefile(folderoutput,objfolderinput,objnodecmd,content):
    from os.path import exists
    from os import mkdir

    if not exists(folderoutput) :
        mkdir(folderoutput)
    folder_eachnode = folderoutput + objfolderinput.foldername

    if not exists(folder_eachnode) :
        mkdir(folder_eachnode)

    filename = folder_eachnode + objfolderinput.prefix+"_"+objnodecmd.nodeip + '_All.txt'
    if not exists(filename) :
        with open(filename,'w') as writer :
            writer.write(content)
    else:
        with open(filename,'a') as writer :
            writer.write(content)

def call_process(aobjnodecmd):
    from modules.Obj import objNodecmd
    from modules.mylib import Call_SSH,loadSettingPath
    from modules.summary_excel import call_summaryexcel
    from modules.parse_log_nokia import split_log_nokia,toExcel

    if isinstance(aobjnodecmd,objNodecmd):

        objsettingpath = loadSettingPath()
        outputcommand = Call_SSH(aobjnodecmd)
        objfolderinput = aobjnodecmd.objfolderinput
        writefile(folderoutput=objsettingpath.folderoutput,objfolderinput=objfolderinput,objnodecmd=aobjnodecmd,content=outputcommand)
        split_log_nokia(objfolderinput=objfolderinput)
        toExcel(objfolderinput=objfolderinput)
        call_summaryexcel(objfolderinput=objfolderinput)

def Run():
    from modules import mylib
    from modules.Obj import objFolderInput
    from multiprocessing import Pool,cpu_count
    prompt_startprogram()
    objsettingpath = mylib.loadSettingPath()
    path_authencisco = objsettingpath.fileauthen_cisco
    path_authensam = objsettingpath.fileauthen_sam
    folderAuthen = objsettingpath.folder_Authen


    autheninfo = mylib.loadAuthen(fileauthen_cisco=path_authencisco,fileauthen_sam=path_authensam,folderauthen=folderAuthen)

    objfolderinput_list = mylib.loadINPUT()
    objnodecmd_list = []
    for eachfolder in objfolderinput_list :
        if isinstance(eachfolder,objFolderInput) :
            for eachnode in eachfolder.filenodelist :
                if 'summary' in eachnode :

                    continue
                objnodecmd = mylib.parseinput_To_objNodecmd(objfolderinput=eachfolder,filenodecmd=eachnode)
                objnodecmd.autheninfo = autheninfo
                objnodecmd.objfolderinput = eachfolder
                objnodecmd_list.append(objnodecmd)




    numberofCPU = cpu_count()
    print(f'CPUs : {numberofCPU}')

    pool = Pool(processes=numberofCPU - 1)
    pool.map(call_process, objnodecmd_list, chunksize=1)
    pool.close()
    pool.join()




if __name__ == '__main__':
    isLAB = True

    Run()

    input("Completed.")